import typing as t

from sqlmodel import Field, SQLModel


class User(SQLModel, table=True):
    __tablename__: str = 'users'

    id: t.Optional[int] = Field(default=None, primary_key=True)
    username: str = Field(index=True, nullable=False)
    email: str = Field(index=True, nullable=False)
    age: t.Optional[int] = Field(default=None, index=True)


class UserCreate(SQLModel):
    username: str
    email: str
    age: t.Optional[int] = None


class UserRead(SQLModel):
    id: int
    username: str
    email: str
    age: t.Optional[int] = None
