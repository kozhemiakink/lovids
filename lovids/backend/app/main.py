import sys
import functools as ft

from fastapi import FastAPI, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession
from sqlmodel import select

from app.db import get_session, User, UserCreate, UserRead
from app.core.config import Settings

if sys.platform == 'win32':
    import trio
    import hypercorn
    from hypercorn.trio import serve
else:
    import uvicorn

app = FastAPI()


@ft.lru_cache()
def get_settings():
    return Settings()


@app.get('/api/ping')
async def ping():
    return {'ping': 'pong'}


@app.get('/api/hello/{name}')
async def say_hello(name: str):
    return {'message': f'Hello {name}'}


@app.get("/api/info")
async def info(settings: Settings = Depends(get_settings)):
    return {
        "app_name": settings.app_name,
        "admin_email": settings.admin_email,
        "items_per_user": settings.items_per_user,
    }


@app.get("/api/users", response_model=list[UserRead])
async def get_users(session: AsyncSession = Depends(get_session)):
    result = await session.execute(select(User))
    users = result.scalars().all()
    return users


@app.post("/api/users", response_model=UserRead, status_code=status.HTTP_201_CREATED)
async def add_user(user: UserCreate, session: AsyncSession = Depends(get_session)):
    db_user = User.from_orm(user)
    session.add(db_user)
    await session.commit()
    await session.refresh(db_user)
    return db_user


if __name__ == '__main__':
    if sys.platform == 'win32':
        config = hypercorn.Config()
        config.bind = ['0.0.0.0:8888']
        config.use_reloader = True
        trio.run(serve, app, config)
    else:
        uvicorn.run('main:app', host='0.0.0.0', reload=True, port=8888)
