import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = 'lovids'
    admin_email: str = 'kozhemiakink@gmail.com'
    items_per_user: int = 50

    POSTGRES_HOST: str = os.getenv('POSTGRES_HOST', 'localhost')
    POSTGRES_PORT: str = os.getenv('POSTGRES_PORT', 5432)
    POSTGRES_DB: str = os.environ['POSTGRES_DB']
    POSTGRES_USER: str = os.environ['POSTGRES_USER']
    POSTGRES_PASSWORD: str = os.environ['POSTGRES_PASSWORD']

    DATABASE_URI = (
        f'postgresql+asyncpg://{POSTGRES_USER}:{POSTGRES_PASSWORD}@'
        f'{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}'
    )


settings = Settings()
